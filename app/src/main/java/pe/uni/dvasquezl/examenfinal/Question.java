package pe.uni.dvasquezl.examenfinal;

public class Question {
    String question;
    Integer imageId;
    Boolean answer;

    public Question(String q, Integer id, Boolean ans){
        question = q;
        imageId = id;
        answer = ans;
    }
}
