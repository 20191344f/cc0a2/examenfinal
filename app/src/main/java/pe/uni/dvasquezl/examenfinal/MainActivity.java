package pe.uni.dvasquezl.examenfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Resources;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button next;
    Button back;
    Button isTrue;
    Button isFalse;
    int cont;
    ArrayList<Question> questions;

    TextView questionNumberShow;

    //
    TextView question;
    ImageView imageQuestion;
    boolean correctAnswer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        question = findViewById(R.id.text_view_question);
        questionNumberShow = findViewById(R.id.question_number_indicator);
        imageQuestion = findViewById(R.id.image_question);
        next = findViewById(R.id.next_option);
        back = findViewById(R.id.back_option);
        isTrue = findViewById(R.id.yes_option);
        isFalse = findViewById(R.id.no_option);

        cont = 0;
        initQuestionArray();
        setQuestionInView();

        next.setOnClickListener(view -> {
            if (cont + 1 < questions.size()) {
                cont++;
            } else {
                cont = 0;
            }
            setQuestionInView();
        });

        back.setOnClickListener(view -> {
            if (cont - 1 > -1) {
                cont--;
                setQuestionInView();
            }
        });

        isFalse.setOnClickListener(view -> {
            RelativeLayout relativeLayout;
            relativeLayout = findViewById(R.id.main_activity_view);

            int message;
            if (!correctAnswer) {
                message = R.string.correct_answer;
            } else {
                message = R.string.incorrect_answer;
            }

            Snackbar.make(relativeLayout, message, Snackbar.LENGTH_SHORT).show();
        });

        isTrue.setOnClickListener(view -> {
            RelativeLayout relativeLayout;
            relativeLayout = findViewById(R.id.main_activity_view);

            int message;
            if (correctAnswer) {
                message = R.string.correct_answer;
            } else {
                message = R.string.incorrect_answer;
            }

            Snackbar.make(relativeLayout, message, Snackbar.LENGTH_SHORT).show();
        });

    }

    void initQuestionArray() {
        questions = new ArrayList<>();
        // inicializa el array de preguntas
        System.out.println("Estoy aca funcionando :c");
        Resources res = getResources();
        questions.add(new Question(String.valueOf(res.getString(R.string.question_1)), R.drawable.ceviche, true));
        questions.add(new Question(String.valueOf(res.getString(R.string.question_2)), R.drawable.rice, false));
        questions.add(new Question(String.valueOf(res.getString(R.string.question_3)), R.drawable.cielo, true));
        questions.add(new Question(String.valueOf(res.getString(R.string.question_4)), R.drawable.persons, false));
        questions.add(new Question(String.valueOf(res.getString(R.string.question_5)), R.drawable.tree, false));
        questions.add(new Question(String.valueOf(res.getString(R.string.question_6)), R.drawable.apples, true));
    }

    void setQuestionInView() {
        // se encarga de cargar el i-esimo objeto del arreglo de preguntas
        // usa el indicador
        Resources res = getResources();
        questionNumberShow.setText(String.format(res.getString(R.string.question_number), cont + 1));

        question.setText(questions.get(cont).question);
        imageQuestion.setImageResource(questions.get(cont).imageId);
        correctAnswer = questions.get(cont).answer;
    }

}